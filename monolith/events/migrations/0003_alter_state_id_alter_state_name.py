# Generated by Django 4.0.3 on 2023-10-17 07:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_picture_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='state',
            name='id',
            field=models.PositiveIntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='state',
            name='name',
            field=models.CharField(max_length=40),
        ),
    ]
